// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_HealthComponent.h"

// Sets default values for this component's properties
UTDS_HealthComponent::UTDS_HealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTDS_HealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UTDS_HealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	
}

float UTDS_HealthComponent::GetCurrentHealth()
{
	return Health;
}

void UTDS_HealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

void UTDS_HealthComponent::ChangeHealthValue(float ChangeValue)
{
	ChangeValue = ChangeValue * CoefDamage;

	Health += ChangeValue;

	OnHealthChange.Broadcast(Health, ChangeValue);
	
	if (Health > 100.0f)
	{
		Health = 100.0f;
	}
	else
	{
		if (Health < 0.0f)
		{
			OnDead.Broadcast();
		}
	}
}


