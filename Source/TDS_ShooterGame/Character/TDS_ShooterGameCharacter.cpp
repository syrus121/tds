// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_ShooterGameCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"
#include "TDS_ShooterGame/TDS_GameInstance.h"
#include "TDS_ShooterGame/Weapon/Projectile/ProjectileDefault.h"


ATDS_ShooterGameCharacter::ATDS_ShooterGameCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UTDSInventoryComponent>(TEXT("InventoryComponent"));
	CharHealthComponent = CreateDefaultSubobject<UTDS_CharacterHealthComponent>(TEXT("HealthComponent"));

	if (CharHealthComponent)
	{
		CharHealthComponent->OnDead.AddDynamic(this, &ATDS_ShooterGameCharacter::CharDead);
	}
	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATDS_ShooterGameCharacter::InitWeapon);
	}
	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDS_ShooterGameCharacter::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (CurrentCursor)
	{
		APlayerController* myPC = Cast<APlayerController>(GetController());
		if (myPC)
		{
			FHitResult TraceHitResult;
			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();

			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
			CurrentCursor->SetWorldRotation(CursorR);
		}
	}

	MovementTick(DeltaSeconds);
}

void ATDS_ShooterGameCharacter::BeginPlay()
{
	Super::BeginPlay();

	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
	}
}

void ATDS_ShooterGameCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
{
	Super::SetupPlayerInputComponent(NewInputComponent);

	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDS_ShooterGameCharacter::InputAxisX);
	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDS_ShooterGameCharacter::InputAxisY);

	NewInputComponent->BindAction(TEXT("ToSprint"), EInputEvent::IE_Pressed, this, &ATDS_ShooterGameCharacter::InputSprintPressed);
	NewInputComponent->BindAction(TEXT("ChangeToWalk"), EInputEvent::IE_Pressed, this, &ATDS_ShooterGameCharacter::InputWalkPressed);
	NewInputComponent->BindAction(TEXT("AimEvent"), EInputEvent::IE_Pressed, this, &ATDS_ShooterGameCharacter::InputAimPressed);
	NewInputComponent->BindAction(TEXT("ToSprint"), EInputEvent::IE_Released, this, &ATDS_ShooterGameCharacter::InputSprintReleased);
	NewInputComponent->BindAction(TEXT("ChangeToWalk"), EInputEvent::IE_Released, this, &ATDS_ShooterGameCharacter::InputWalkReleased);
	NewInputComponent->BindAction(TEXT("AimEvent"), EInputEvent::IE_Released, this, &ATDS_ShooterGameCharacter::InputAimReleased);

	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATDS_ShooterGameCharacter::InputAttackPressed);
	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATDS_ShooterGameCharacter::InputAttackReleased);
	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATDS_ShooterGameCharacter::TryReloadWeapon);

	NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ATDS_ShooterGameCharacter::TrySwitchNextWeapon);
	NewInputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &ATDS_ShooterGameCharacter::TrySwitchPreviosWeapon);

	NewInputComponent->BindAction(TEXT("AblityAction"), EInputEvent::IE_Pressed, this, &ATDS_ShooterGameCharacter::TryAbilityEnabled);

	NewInputComponent->BindAction(TEXT("DropCurrentWeapon"), EInputEvent::IE_Pressed, this, &ATDS_ShooterGameCharacter::DropCurrentWeapon);

	TArray<FKey> HotKeys;
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);
	HotKeys.Add(EKeys::Zero);

	NewInputComponent->BindKey(HotKeys[1], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<1>);
	NewInputComponent->BindKey(HotKeys[2], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<2>);
	NewInputComponent->BindKey(HotKeys[3], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<3>);
	NewInputComponent->BindKey(HotKeys[4], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<4>);
	NewInputComponent->BindKey(HotKeys[5], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<5>);
	NewInputComponent->BindKey(HotKeys[6], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<6>);
	NewInputComponent->BindKey(HotKeys[7], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<7>);
	NewInputComponent->BindKey(HotKeys[8], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<8>);
	NewInputComponent->BindKey(HotKeys[9], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<9>);
	NewInputComponent->BindKey(HotKeys[0], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<0>);
}

void ATDS_ShooterGameCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATDS_ShooterGameCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATDS_ShooterGameCharacter::InputAttackPressed()
{
	if (bIsAlive)
	{
		AttackCharEvent(true);
	}
}

void ATDS_ShooterGameCharacter::InputAttackReleased()
{
	AttackCharEvent(false);
}

void ATDS_ShooterGameCharacter::InputWalkPressed()
{
	WalkEnabled = true;
	ChangeMovementState();
}

void ATDS_ShooterGameCharacter::InputWalkReleased()
{
	WalkEnabled = false;
	ChangeMovementState();
}

void ATDS_ShooterGameCharacter::InputSprintPressed()
{
	SprintRunEnabled = true;
	ChangeMovementState();
}

void ATDS_ShooterGameCharacter::InputSprintReleased()
{
	SprintRunEnabled = false;
	ChangeMovementState();
}

void ATDS_ShooterGameCharacter::InputAimPressed()
{
	AimEnabled = true;
	ChangeMovementState();
}

void ATDS_ShooterGameCharacter::InputAimReleased()
{
	AimEnabled = false;
	ChangeMovementState();
}

void ATDS_ShooterGameCharacter::MovementTick(float DeltaTime)
{
	if (bIsAlive)
	{
		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);

		if (MovementState == EMovementState::SprintRun_State)
		{
			FVector myRotationVector = FVector(AxisX, AxisY, 0.0f);
			FRotator myRotator = myRotationVector.ToOrientationRotator();
			SetActorRotation((FQuat(myRotator)));
		}
		else
		{
			APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
			if (myController)
			{
				FHitResult ResultHit;
				//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);// bug was here Config\DefaultEngine.Ini
				myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);

				float FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
				SetActorRotation(FQuat(FRotator(0.0f, FindRotaterResultYaw, 0.0f)));

				if (CurrentWeapon)
				{
					FVector Displacement = FVector(0);
					switch (MovementState)
					{
					case EMovementState::Aim_State:
						Displacement = FVector(0.0f, 0.0f, 160.0f);
						CurrentWeapon->ShouldReduceDispersion = true;
						break;
					case EMovementState::AimWalk_State:
						CurrentWeapon->ShouldReduceDispersion = true;
						Displacement = FVector(0.0f, 0.0f, 160.0f);
						break;
					case EMovementState::Walk_State:
						Displacement = FVector(0.0f, 0.0f, 120.0f);
						CurrentWeapon->ShouldReduceDispersion = false;
						break;
					case EMovementState::Run_State:
						Displacement = FVector(0.0f, 0.0f, 120.0f);
						CurrentWeapon->ShouldReduceDispersion = false;
						break;
					case EMovementState::SprintRun_State:
						break;
					default:
						break;
					}

					CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
					//aim cursor like 3d Widget?
				}
			}
		}
	}
}

EMovementState ATDS_ShooterGameCharacter::GetMovementState()
{
	return MovementState;
}

TArray<UStateEffect*> ATDS_ShooterGameCharacter::GetCurrentEffectsOnChar()
{
	return Effects;
}

int32 ATDS_ShooterGameCharacter::GetCurrentWeaponIndex()
{
	return CurrentIndexWeapon;
}

void ATDS_ShooterGameCharacter::AttackCharEvent(bool bIsFiring)
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->SetWeaponStateFire(bIsFiring);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("ATDS_ShooterGameCharacter::AttackCharEvent - CurrentWeapon -NULL"));
}

void ATDS_ShooterGameCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementSpeedInfo.AimSpeedNormal;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementSpeedInfo.AimSpeedWalk;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementSpeedInfo.WalkSpeedNormal;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementSpeedInfo.RunSpeedNormal;
		break;
	case EMovementState::SprintRun_State:
		ResSpeed = MovementSpeedInfo.SprintRunSpeedRun;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDS_ShooterGameCharacter::ChangeMovementState()
{
	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
	{
		MovementState = EMovementState::Run_State;
	}
	else
	{
		if (SprintRunEnabled)
		{
			WalkEnabled = false;
			AimEnabled = false;
			MovementState = EMovementState::SprintRun_State;
		}
		if (WalkEnabled && !SprintRunEnabled && AimEnabled)
		{
			MovementState = EMovementState::AimWalk_State;
		}
		else
		{
			if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
			{
				MovementState = EMovementState::Walk_State;
			}
			else
			{
				if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
				{
					MovementState = EMovementState::Aim_State;
				}
			}
		}
	}
	CharacterUpdate();

	//Weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}
}

AWeaponDefault* ATDS_ShooterGameCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}

void ATDS_ShooterGameCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UTDS_GameInstance* myGI = Cast<UTDS_GameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				SpawnParams.Instigator = GetInstigator();

				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;

					myWeapon->IdWeaponName = IdWeaponName;
					myWeapon->WeaponSetting = myWeaponInfo;


					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
					myWeapon->UpdateStateWeapon(MovementState);

					myWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;

					CurrentIndexWeapon = NewCurrentIndexWeapon;

					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDS_ShooterGameCharacter::WeaponReloadStart);
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDS_ShooterGameCharacter::WeaponReloadEnd);

					myWeapon->OnWeaponFireStart.AddDynamic(this, &ATDS_ShooterGameCharacter::WeaponFireStart);

					// after switch try reload weapon if needed
					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
						CurrentWeapon->InitReload();

					if (InventoryComponent)
						InventoryComponent->OnWeaponAmmoAviable.Broadcast(myWeapon->WeaponSetting.WeaponType);
				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("ATDS_ShooterGameCharacter::InitWeapon - Weapon not found in table -NULL"));
		}
	}
}



void ATDS_ShooterGameCharacter::TryReloadWeapon()
{
	if (bIsAlive && CurrentWeapon && !CurrentWeapon->WeaponReloading)
	{
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
			CurrentWeapon->InitReload();
	}
}

void ATDS_ShooterGameCharacter::WeaponReloadStart(UAnimMontage* Anim)
{
	WeaponReloadStart_BP(Anim);
}

void ATDS_ShooterGameCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (InventoryComponent && CurrentWeapon)
	{
		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	}
	WeaponReloadEnd_BP(bIsSuccess);
}

bool ATDS_ShooterGameCharacter::TrySwitchWeaponToIndexByKeyInput(int32 ToIndex)
{
	bool bIsSuccess = false;
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.IsValidIndex(ToIndex))
	{
		if (CurrentIndexWeapon != ToIndex && InventoryComponent)
		{
			int32 OldIndex = CurrentIndexWeapon;
			FAdditionalWeaponInfo OldInfo;

			if (CurrentWeapon)
			{
				OldInfo = CurrentWeapon->AdditionalWeaponInfo;
				if (CurrentWeapon->WeaponReloading)
					CurrentWeapon->CancelReload();
			}

			bIsSuccess = InventoryComponent->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
		}
	}
	return bIsSuccess;
}

void ATDS_ShooterGameCharacter::DropCurrentWeapon()
{
	if (InventoryComponent)
	{
		FDropItem ItemInfo;
		InventoryComponent->DropWeaponByIndex(CurrentIndexWeapon, ItemInfo);
	}
}

void ATDS_ShooterGameCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

void ATDS_ShooterGameCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	// in BP
}

void ATDS_ShooterGameCharacter::WeaponFireStart(UAnimMontage* Anim)
{
	if (InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
	WeaponFireStart_BP(Anim);
}

void ATDS_ShooterGameCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
{
	// in BP
}

UDecalComponent* ATDS_ShooterGameCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}

void ATDS_ShooterGameCharacter::TrySwitchNextWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			if (InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
			{
			}
		}
	}
}

void ATDS_ShooterGameCharacter::TrySwitchPreviosWeapon()
{
	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
			if (CurrentWeapon->WeaponReloading)
				CurrentWeapon->CancelReload();
		}

		if (InventoryComponent)
		{
			//InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->AdditionalWeaponInfo);
			if (InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
			{
			}
		}
	}
}


void ATDS_ShooterGameCharacter::TryAbilityEnabled()
{
	if (AbilityEffect)//TODO Cool down
	{
		UStateEffect* NewEffect = NewObject<UStateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this);
		}
	}
}

EPhysicalSurface ATDS_ShooterGameCharacter::GetSurfaceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	if (CharHealthComponent)
	{
		if (CharHealthComponent->GetCurrentShield() <= 0)
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}
	return Result;
}

TArray<UStateEffect*> ATDS_ShooterGameCharacter::GetAllCurrentEffects()
{
	return Effects;
}

void ATDS_ShooterGameCharacter::RemoveEffect(UStateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void ATDS_ShooterGameCharacter::AddEffect(UStateEffect* newEffect)
{
	Effects.Add(newEffect);
}

void ATDS_ShooterGameCharacter::CharDead_BP_Implementation()
{
	//BP
}

void ATDS_ShooterGameCharacter::CharDead()
{
	float TimeAnim = 0.0f;
	int32 rnd = FMath::RandHelper(DeadsAnim.Num());
	if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
		TimeAnim = DeadsAnim[rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
	}

	bIsAlive = false;

	if (GetController())
	{
		GetController()->UnPossess();
	}

	UnPossessed();

	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATDS_ShooterGameCharacter::EnableRagdoll, TimeAnim, false);

	GetCursorToWorld()->SetVisibility(false);

	AttackCharEvent(false);

	CharDead_BP();
}

void ATDS_ShooterGameCharacter::EnableRagdoll()
{
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

float ATDS_ShooterGameCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (bIsAlive)
	{
		CharHealthComponent->ChangeHealthValue(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, myProjectile->ProjectileSetting.Effect, GetSurfaceType());
		}
	}

	return ActualDamage;
}

//ATDS_ShooterGameCharacter::ATDS_ShooterGameCharacter()
//{
//	// Set size for player capsule
//	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
//
//	// Don't rotate character to camera direction
//	bUseControllerRotationPitch = false;
//	bUseControllerRotationYaw = false;
//	bUseControllerRotationRoll = false;
//
//	// Configure character movement
//	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
//	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
//	GetCharacterMovement()->bConstrainToPlane = true;
//	GetCharacterMovement()->bSnapToPlaneAtStart = true;
//
//	// Create a camera boom...
//	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
//	CameraBoom->SetupAttachment(RootComponent);
//	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
//	CameraBoom->TargetArmLength = 800.f;
//	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
//	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level
//
//	// Create a camera...
//	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
//	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
//	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm
//
//	InventoryComponent = CreateDefaultSubobject<UTDSInventoryComponent>(TEXT("InventoryComponent"));
//	CharHealthComponent = CreateDefaultSubobject<UTDS_CharacterHealthComponent>(TEXT("HealthComponent"));
//	if (CharHealthComponent)
//	{
//		CharHealthComponent->OnDead.AddDynamic(this, &ATDS_ShooterGameCharacter::CharDead);
//	}
//	if (InventoryComponent)
//	{
//		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &ATDS_ShooterGameCharacter::InitWeapon);
//	}
//
//	// Activate ticking in order to update the cursor every frame.
//	PrimaryActorTick.bCanEverTick = true;
//	PrimaryActorTick.bStartWithTickEnabled = true;
//}
//
//void ATDS_ShooterGameCharacter::Tick(float DeltaSeconds)
//{
//	Super::Tick(DeltaSeconds);
//
//		if (CurrentCursor)
//	{
//		APlayerController* myPC = Cast<APlayerController>(GetController());
//		if (myPC)
//		{
//			FHitResult TraceHitResult;
//			myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
//			FVector CursorFV = TraceHitResult.ImpactNormal;
//			FRotator CursorR = CursorFV.Rotation();
//
//			CurrentCursor->SetWorldLocation(TraceHitResult.Location);
//			CurrentCursor->SetWorldRotation(CursorR);
//		}
//	}
//	MovementTick(DeltaSeconds);
//}
//
//void ATDS_ShooterGameCharacter::BeginPlay()
//{
//	Super::BeginPlay();
//	
//	if (CursorMaterial)
//	{
//		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
//	}
//}
//
//void ATDS_ShooterGameCharacter::SetupPlayerInputComponent(UInputComponent* NewInputComponent)
//{
//	Super::SetupPlayerInputComponent(NewInputComponent);
//
//	NewInputComponent->BindAxis(TEXT("MoveForward"), this, &ATDS_ShooterGameCharacter::InputAxisX);
//	NewInputComponent->BindAxis(TEXT("MoveRight"), this, &ATDS_ShooterGameCharacter::InputAxisY);
//
//	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &ATDS_ShooterGameCharacter::InputAttackPressed);
//	NewInputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &ATDS_ShooterGameCharacter::InputAttackReleased);
//	NewInputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &ATDS_ShooterGameCharacter::TryReloadWeapon);
//	
//	NewInputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Pressed, this, &ATDS_ShooterGameCharacter::TrySwitchNextWeapon);
//	NewInputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Pressed, this, &ATDS_ShooterGameCharacter::TrySwitchPreviosWeapon);
//	
//	NewInputComponent->BindAction(TEXT("AbilityAction"), EInputEvent::IE_Pressed, this, &ATDS_ShooterGameCharacter::TryAbilityEnabled);
//
//	NewInputComponent->BindAction(TEXT("DropCurrentWeapon"), EInputEvent::IE_Pressed, this, &ATDS_ShooterGameCharacter::DropCurrentWeapon);
//}
//
//TArray<FKey> HotKeys;
//HotKeys.Add(EKeys::One);
//HotKeys.Add(EKeys::Two);
//HotKeys.Add(EKeys::Three);
//HotKeys.Add(EKeys::Four);
//HotKeys.Add(EKeys::Five);
//HotKeys.Add(EKeys::Six);
//HotKeys.Add(EKeys::Seven);
//HotKeys.Add(EKeys::Eight);
//HotKeys.Add(EKeys::Nine);
//HotKeys.Add(EKeys::Zero);
//
//NewInputComponent->BindKey(HotKeys[1], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<1>);
//NewInputComponent->BindKey(HotKeys[2], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<2>);
//NewInputComponent->BindKey(HotKeys[3], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<3>);
//NewInputComponent->BindKey(HotKeys[4], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<4>);
//NewInputComponent->BindKey(HotKeys[5], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<5>);
//NewInputComponent->BindKey(HotKeys[6], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<6>);
//NewInputComponent->BindKey(HotKeys[7], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<7>);
//NewInputComponent->BindKey(HotKeys[8], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<8>);
//NewInputComponent->BindKey(HotKeys[9], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<9>);
//NewInputComponent->BindKey(HotKeys[0], IE_Pressed, this, &ATDS_ShooterGameCharacter::TKeyPressed<0>);
//
//
//
//void ATDS_ShooterGameCharacter::InputAxisY(float Value)
//{
//	AxisY = Value;
//}
//
//void ATDS_ShooterGameCharacter::InputAxisX(float Value)
//{
//	AxisX = Value;
//}
//
//void ATDS_ShooterGameCharacter::InputAttackPressed()
//{
//	AttackCharEvent(true);
//}
//
//void ATDS_ShooterGameCharacter::InputAttackReleased()
//{
//	AttackCharEvent(false);
//}
//
//
//void ATDS_ShooterGameCharacter::MovementTick(float DeltaTime)
//{
//	if (bIsAlive)
//	{
//		AddMovementInput(FVector(1.0f, 0.0f, 0.0f), AxisX);
//		AddMovementInput(FVector(0.0f, 1.0f, 0.0f), AxisY);
//
//		if (MovementState == EMovementState::SprintRun_State)
//		{
//			FVector myRotationVector = FVector(AxisX, AxisY, 0.0f);
//			FRotator myRotator = myRotationVector.ToOrientationRotator();
//			SetActorRotation((FQuat(myRotator)));
//		}
//		else
//		{
//
//			APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
//			if (myController)
//			{
//				FHitResult ResultHit;
//				//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
//				myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);
//
//				float FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
//				SetActorRotation(FQuat(FRotator(0.0f, FindRotaterResultYaw, 0.0f)));
//				if (CurrentWeapon)
//				{
//					FVector Displacement = FVector(0);
//					switch (MovementState)
//					{
//					case EMovementState::Aim_State:
//						Displacement = FVector(0.0f, 0.0f, 160.0f);
//						CurrentWeapon->ShouldReduceDispersion = true;
//						break;
//					case EMovementState::AimWalk_State:
//						CurrentWeapon->ShouldReduceDispersion = true;
//						Displacement = FVector(0.0f, 0.0f, 160.0f);
//						break;
//					case EMovementState::Walk_State:
//						Displacement = FVector(0.0f, 0.0f, 120.0f);
//						CurrentWeapon->ShouldReduceDispersion = false;
//						break;
//					case EMovementState::Run_State:
//						Displacement = FVector(0.0f, 0.0f, 120.0f);
//						CurrentWeapon->ShouldReduceDispersion = false;
//						break;
//					case EMovementState::SprintRun_State:
//						break;
//					default:
//
//						break;
//					}
//
//					CurrentWeapon->ShootEndLocation = ResultHit.Location + Displacement;
//					//	//aim cursor like 3d Widget?
//				}
//			}
//		}
//	}
//
//	
//}
//
//
//EMovementState ATDS_ShooterGameCharacter::GetMovementState()
//{
//	return EMovementState();
//}
//
//TArray<UStateEffect*> ATDS_ShooterGameCharacter::GetCurrentEffectsOnChar()
//{
//	return Effects;
//}
//
//int32 ATDS_ShooterGameCharacter::GetCurrentWeaponIndex()
//{
//	return CurrentIndexWeapon;
//}
//
//	void ATDS_ShooterGameCharacter::AttackCharEvent(bool bIsFiring)
//{
//	AWeaponDefault* myWeapon = nullptr;
//	myWeapon = GetCurrentWeapon();
//	if (myWeapon)
//	{
//		//ToDo Check melee or range
//		myWeapon->SetWeaponStateFire(bIsFiring);
//	}
//	else
//		UE_LOG(LogTemp, Warning, TEXT("ATDS_ShooterGameCharacter::AttackCharEvent - CurrentWeapon -NULL"));
//}
//
//
//	
//
//
//void ATDS_ShooterGameCharacter::CharacterUpdate()
//{
//	float ResSpeed = 600.0f;
//	switch (MovementState)
//	{
//	case EMovementState::Aim_State:
//		ResSpeed = MovementSpeedInfo.AimSpeedNormal;
//		break;
//	case EMovementState::AimWalk_State:
//		ResSpeed = MovementSpeedInfo.AimSpeedWalk;
//		break;
//	case EMovementState::Walk_State:
//		ResSpeed = MovementSpeedInfo.WalkSpeedNormal;
//		break;
//	case EMovementState::Run_State:
//		ResSpeed = MovementSpeedInfo.RunSpeedNormal;
//		break;
//	case EMovementState::SprintRun_State:
//		ResSpeed = MovementSpeedInfo.SprintRunSpeedRun;
//		break;
//	default:
//		break;
//	}
//
//	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
//}
//
//void ATDS_ShooterGameCharacter::ChangeMovementState()
//{
//	if (!WalkEnabled && !SprintRunEnabled && !AimEnabled)
//	{
//		MovementState = EMovementState::Run_State;
//	}
//	else
//	{
//		if (SprintRunEnabled)
//		{
//			WalkEnabled = false;
//			AimEnabled = false;
//			MovementState = EMovementState::SprintRun_State;
//		}
//		if (WalkEnabled && !SprintRunEnabled && AimEnabled)
//		{
//			MovementState = EMovementState::AimWalk_State;
//		}
//		else
//		{
//			if (WalkEnabled && !SprintRunEnabled && !AimEnabled)
//			{
//				MovementState = EMovementState::Walk_State;
//			}
//			else
//			{
//				if (!WalkEnabled && !SprintRunEnabled && AimEnabled)
//				{
//					MovementState = EMovementState::Aim_State;
//				}
//			}
//		}
//	}
//	CharacterUpdate();
//	//Weapon state update
//	AWeaponDefault* myWeapon = GetCurrentWeapon();
//	if (myWeapon)
//	{
//		myWeapon->UpdateStateWeapon(MovementState);
//	}
//
//}
//
//AWeaponDefault* ATDS_ShooterGameCharacter::GetCurrentWeapon()
//{
//	return CurrentWeapon;
//}
//
//void ATDS_ShooterGameCharacter::InitWeapon(FName IdWeaponName, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
//{
//	if (CurrentWeapon)
//	{
//		CurrentWeapon->Destroy();
//		CurrentWeapon = nullptr;
//	}
//	UTDS_GameInstance* myGI = Cast<UTDS_GameInstance>(GetGameInstance());
//	FWeaponInfo myWeaponInfo;
//	if (myGI)
//	{
//		if(myGI->GetWeaponInfoByName(IdWeaponName, myWeaponInfo))
//		{
//			if (myWeaponInfo.WeaponClass)
//			{
//				FVector SpawnLocation = FVector(0);
//				FRotator SpawnRotation = FRotator(0);
//
//				FActorSpawnParameters SpawnParams;
//				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
//				SpawnParams.Owner = this;
//				SpawnParams.Instigator = GetInstigator();
//
//				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
//				if (myWeapon)
//				{
//					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
//					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
//					CurrentWeapon = myWeapon;
//					
//					myWeapon->IdWeaponName = IdWeaponName;
//					myWeapon->WeaponSetting = myWeaponInfo;
//					
//					myWeapon->ReloadTime = myWeaponInfo.ReloadTime;
//					myWeapon->UpdateStateWeapon(MovementState);
//
//					myWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;
//					//if (InventoryComponent)
//					CurrentIndexWeapon = NewCurrentIndexWeapon;
//
//					myWeapon->OnWeaponReloadStart.AddDynamic(this, &ATDS_ShooterGameCharacter::WeaponReloadStart);
//					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &ATDS_ShooterGameCharacter::WeaponReloadEnd);
//					
//					myWeapon->OnWeaponFireStart.AddDynamic(this, &ATDS_ShooterGameCharacter::WeaponFireStart);
//
//					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
//						CurrentWeapon->InitReload();
//
//					if (InventoryComponent)
//						InventoryComponent->OnWeaponAmmoAviable.Broadcast(myWeapon->WeaponSetting.WeaponType);
//				}
//			}
//		}
//		else
//		{
//			UE_LOG(LogTemp, Warning, TEXT("ATDS_ShooterGameCharacter::InitWeapon - Weapon not found in table -NULL"));
//		}
//	}
//
//	
//}
//
//void ATDS_ShooterGameCharacter::RemoveCurrentWeapon()
//{
//}
//
//void ATDS_ShooterGameCharacter::TryReloadWeapon()
//{
//	if (CurrentWeapon && !CurrentWeapon->WeaponReloading) // ���� ���� � ������������ ������
//	{
//		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload())
//			CurrentWeapon->InitReload();
//	}
//}
//
//
//
//void ATDS_ShooterGameCharacter::WeaponReloadStart(UAnimMontage* Anim)
//{
//	WeaponReloadStart_BP(Anim);
//}
//
//void ATDS_ShooterGameCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
//{
//	if (InventoryComponent && CurrentWeapon)
//	{
//		InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.WeaponType, AmmoTake);
//		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
//	}
//
//	WeaponReloadEnd_BP(bIsSuccess);
//}
//
//
//
//void ATDS_ShooterGameCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* Anim)
//{
//	// in BP
//}
//
//void ATDS_ShooterGameCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
//{
//	// in BP
//}
//
//void ATDS_ShooterGameCharacter::WeaponFireStart(UAnimMontage* Anim)
//{
//	if (InventoryComponent && CurrentWeapon)
//		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
//
//	WeaponFireStart_BP(Anim);
//}
//
//void ATDS_ShooterGameCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* Anim)
//{
//	// in BP
//}
//
//UDecalComponent* ATDS_ShooterGameCharacter::GetCursorToWorld()
//{
//	return CurrentCursor;
//}
//
//
//void ATDS_ShooterGameCharacter::TrySwitchNextWeapon()
//{
//	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
//	{
//		//We have more then one weapon go switch
//		int8 OldIndex = CurrentIndexWeapon;
//		FAdditionalWeaponInfo OldInfo;
//		if (CurrentWeapon)
//		{
//			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
//			if (CurrentWeapon->WeaponReloading)
//				CurrentWeapon->CancelReload();
//		}
//
//		if (InventoryComponent)
//		{
//			if (InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon + 1, OldIndex, OldInfo, true))
//			{
//			}
//		}
//	}
//}
//
//void ATDS_ShooterGameCharacter::TrySwitchPreviosWeapon()
//{
//	if (CurrentWeapon && !CurrentWeapon->WeaponReloading && InventoryComponent->WeaponSlots.Num() > 1)
//	{
//		//We have more then one weapon go switch
//		int8 OldIndex = CurrentIndexWeapon;
//		FAdditionalWeaponInfo OldInfo;
//		if (CurrentWeapon)
//		{
//			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
//			if (CurrentWeapon->WeaponReloading)
//				CurrentWeapon->CancelReload();
//		}
//
//		if (InventoryComponent)
//		{
//			//InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->AdditionalWeaponInfo);
//			if (InventoryComponent->SwitchWeaponToIndexByNextPreviosIndex(CurrentIndexWeapon - 1, OldIndex, OldInfo, false))
//			{
//			}
//		}
//	}
//}
//
//bool ATDS_ShooterGameCharacter::TrySwitchWeaponToIndexByKeyInput(int32 ToIndex)
//{
//	bool bIsSuccess = false;
//	if (InventoryComponent->WeaponSlots.IsValidIndex(ToIndex))
//	{
//		if (CurrentIndexWeapon != ToIndex && InventoryComponent)
//		{
//			int32 OldIndex = CurrentIndexWeapon;
//			FAdditionalWeaponInfo OldInfo;
//
//			if (CurrentWeapon)
//			{
//				OldInfo = CurrentWeapon->AdditionalWeaponInfo;
//				if (CurrentWeapon->WeaponReloading)
//					CurrentWeapon->CancelReload();
//			}
//			bIsSuccess = InventoryComponent->SwitchWeaponByIndex(ToIndex, OldIndex, OldInfo);
//		}
//	}
//	return bIsSuccess;
//}
//
//void ATDS_ShooterGameCharacter::DropCurrentWeapon()
//{
//	if (InventoryComponent)
//	{
//		FDropItem ItemInfo;
//		InventoryComponent->DropWeaponByIndex(CurrentIndexWeapon, ItemInfo);
//	}
//}
//
//void ATDS_ShooterGameCharacter::TryAbilityEnabled()
//{
//	if (AbilityEffect)
//	{
//		UStateEffect* NewEffect = NewObject<UStateEffect>(this, AbilityEffect);
//		if (NewEffect)
//		{
//			NewEffect->InitObject(this);
//		}
//	}
//}
//
//
//
//
//
//EPhysicalSurface ATDS_ShooterGameCharacter::GetSurfaceType()
//{
//	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
//	if (CharHealthComponent)
//	{
//		if (CharHealthComponent->GetCurrentShield() <= 0)
//		{
//			if (GetMesh())
//			{
//				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
//				if (myMaterial)
//				{
//					Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
//				}
//			}
//		}
//	}
//	return Result;
//}
//
//TArray<UStateEffect*> ATDS_ShooterGameCharacter::GetAllCurrentEffects()
//{
//	return Effects;
//}
//
//void ATDS_ShooterGameCharacter::RemoveEffect(UStateEffect* RemoveEffect)
//{
//	
//	Effects.Remove(RemoveEffect);
//}
//
//void ATDS_ShooterGameCharacter::AddEffect(UStateEffect* NewEffect)
//{
//	Effects.Add(NewEffect);
//}
//
//void ATDS_ShooterGameCharacter::CharDead()
//{
//	float TimeAnim = 0.0f;
//	int32 rnd = FMath::RandHelper(DeadsAnim.Num());
//	if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
//	{
//		TimeAnim = DeadsAnim[rnd]->GetPlayLength();
//		GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
//	}
//	
//	bIsAlive = false;
//
//	UnPossessed();
//
//	// Timer ragdoll
//	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &ATDS_ShooterGameCharacter::EnableRagdoll, TimeAnim, false);
//	
//	GetCursorToWorld()->SetVisibility(false);
//}
//
//void ATDS_ShooterGameCharacter::EnableRagdoll()
//{
//	if (GetMesh())
//	{
//		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
//		GetMesh()->SetSimulatePhysics(true);
//	}
//}
//
//float ATDS_ShooterGameCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
//{
//	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
//	if (bIsAlive)
//	{
//		CharHealthComponent->ChangeHealthValue(-DamageAmount);
//	}
//	
//	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
//	{
//		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
//		if (myProjectile)
//		{
//			UTypes::AddEffectBySurfaceType(this, myProjectile->ProjectileSetting.Effect, GetSurfaceType());
//			
//		}
//	}
//
//	return ActualDamage;
//}



