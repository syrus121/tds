// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_ShooterGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDS_ShooterGame, "TDS_ShooterGame" );

DEFINE_LOG_CATEGORY(LogTDS_ShooterGame)
 