// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDS_ShooterGameGameMode.generated.h"

UCLASS(minimalapi)
class ATDS_ShooterGameGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	ATDS_ShooterGameGameMode();

	void PlayerCharacterDead();
};



