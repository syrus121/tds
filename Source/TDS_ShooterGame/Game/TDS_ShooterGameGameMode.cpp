// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDS_ShooterGameGameMode.h"
#include "TDS_ShooterGamePlayerController.h"
#include "TDS_ShooterGame/Character/TDS_ShooterGameCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATDS_ShooterGameGameMode::ATDS_ShooterGameGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDS_ShooterGamePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	//static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/TopDownCharacter"));
	/*if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}*/
}

void ATDS_ShooterGameGameMode::PlayerCharacterDead()
{
}
