// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "TDS_ShooterGame/FuncLibrary/Types.h"
#include "Engine/DataTable.h"
#include "TDS_ShooterGame/Weapon/WeaponDefault.h"
#include "TDS_GameInstance.generated.h"

/**
 * 
 */
UCLASS()
class TDS_SHOOTERGAME_API UTDS_GameInstance : public UGameInstance
{
	GENERATED_BODY()


public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Setting")
		UDataTable* WeaponInfoTable = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Setting")
		UDataTable* DropItemInfoTable = nullptr;
	UFUNCTION(BlueprintCallable)
		bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);
	UFUNCTION(BlueprintCallable)
		bool GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo);
	UFUNCTION(BlueprintCallable)
		bool GetDropItemInfoByWeaponName(FName NameItem, FDropItem& OutInfo);
};
